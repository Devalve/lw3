package com.example.lw3;

import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.lw3.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;
    private  String[] result = new String[3];
    List<String> nouns = new ArrayList<>();
    String[] articles = { "за", "над", "под"};
    private String oldText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    nouns.add("заплыв");
    nouns.add("запой");
    nouns.add("мероприятие");
    binding = ActivityMainBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    RadioButton noun1Rb = findViewById(R.id.noun1);
        RadioButton noun2Rb = findViewById(R.id.noun2);
        noun1Rb.setOnClickListener(radioButtonClickListener);
        noun2Rb.setOnClickListener(radioButtonClickListener);
        RadioButton noun3Rb = findViewById(R.id.noun3);
        noun3Rb.setOnClickListener(radioButtonClickListener);
        RadioButton noun4Rb = findViewById(R.id.noun4);
        noun4Rb.setOnClickListener(radioButtonClickListener);

        Spinner spinner = findViewById(R.id.spinner);
        // Создаем адаптер ArrayAdapter с помощью массива строк и стандартной разметки элемета spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, nouns);
        // Определяем разметку для использования при выборе элемента
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Применяем адаптер к элементу spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Spinner spinner = findViewById(R.id.spinner);
                result[2] = spinner.getSelectedItem().toString();
                EditText editText = findViewById(R.id.editText);
                editText.setText(spinner.getSelectedItem().toString());
                showText();
                oldText = spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // получаем элемент ListView
        ListView articleList = findViewById(R.id.countriesList);

        // создаем адаптер
        ArrayAdapter<String> adapter1 = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, articles);

        // устанавливаем для списка адаптер
        articleList.setAdapter(adapter1);

        articleList.setOnItemClickListener((parent, v, position, id) -> {
            // по позиции получаем выбранный элемент
            String selectedItem = articles[position];
            // установка текста элемента TextView
            result[1] = selectedItem;
            showText();
        });
        Button addButton = findViewById(R.id.addButton);
        Button delButton = findViewById(R.id.delButton);

        addButton.setOnClickListener(view -> {
            EditText editText = findViewById(R.id.editText);
            String text = editText.getText().toString();
            nouns.add(text);
            adapter.notifyDataSetChanged();
        });
        delButton.setOnClickListener(view -> {
            EditText editText = findViewById(R.id.editText);
            String text = editText.getText().toString();
            nouns.remove(text);
            adapter.notifyDataSetChanged();
        });
        EditText editText = findViewById(R.id.editText);
        editText.setOnKeyListener((View.OnKeyListener) (view, i, keyEvent) -> {
            if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                if(nouns.contains(oldText)) {
                String text = editText.getText().toString();
                nouns.remove(oldText);
                nouns.add(text);
                adapter.notifyDataSetChanged();
                }
            }
            return true;
        });
    }
        @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    View.OnClickListener radioButtonClickListener = v -> {
        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        RadioButton rb = findViewById(radioGroup.getCheckedRadioButtonId());
        result[0] = rb.getText().toString();
        showText();
    };
    void showText(){
        TextView textView = findViewById(R.id.textView);
        if (result[0] == null || result[1] == null || result[2] == null ) return;
        textView.setText(result[0] + " " + result[1]+ " "   + result[2]);
    }
}